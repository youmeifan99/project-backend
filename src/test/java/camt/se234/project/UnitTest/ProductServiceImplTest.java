package camt.se234.project.UnitTest;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import camt.se234.project.service.ProductServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {
    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setup() {
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void testGetAllProductsWithMock() {
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(new Product("1234","banana","a lot banana","banana.jpg",100.00));
        mockProducts.add(new Product("4231","orange","a lot orange","orange.jpg",100.00));
        mockProducts.add(new Product("4567","rambutan","a lot rambutan","rambutan.jpg",100.00));
        mockProducts.add(new Product("7654","papaya","a lot papaya","papaya.jpg",100.00));
        when(productService.getAllProducts()).thenReturn(mockProducts);
        assertThat(productService.getAllProducts(),hasItem(new Product("1234","banana","a lot banana","banana.jpg",100.00)));
        assertThat(productService.getAllProducts(),hasItems(new Product("1234","banana","a lot banana","banana.jpg",100.00),
                new Product("4231","orange","a lot orange","orange.jpg",100.00),
                new Product("4567","rambutan","a lot rambutan","rambutan.jpg",100.00)));
    }
    @Test
    public void testAvailableProducts() {
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(new Product("1234","banana","a lot banana","banana.jpg",100.00));
        mockProducts.add(new Product("4231","orange","a lot orange","orange.jpg",100.00));
        mockProducts.add(new Product("4567","rambutan","a lot rambutan","rambutan.jpg",0.00));
        mockProducts.add(new Product("7654","papaya","a lot papaya","papaya.jpg",0.00));
        when(productService.getAvailableProducts()).thenReturn(mockProducts);
        assertThat(productService.getAvailableProducts(),hasItems(new Product("1234","banana","a lot banana","banana.jpg",100.00),
                new Product("1234","banana","a lot banana","banana.jpg",100.00)));
        assertThat(productService.getAvailableProducts(),not(hasItems(new Product("4567","rambutan","a lot rambutan","rambutan.jpg",100.00),
                new Product("7654","papaya","a lot papaya","papaya.jpg",100.00))));
    }
    @Test
    public void testUnavailableProducts() {
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(new Product("1234","banana","a lot banana","banana.jpg",100.00));
        mockProducts.add(new Product("4231","orange","a lot orange","orange.jpg",100.00));
        mockProducts.add(new Product("4567","rambutan","a lot rambutan","rambutan.jpg",0.00));
        mockProducts.add(new Product("7654","papaya","a lot papaya","papaya.jpg",0.00));
        when(productService.getAvailableProducts()).thenReturn(mockProducts);
        assertThat(productService.getUnavailableProductSize(),is(2));
    }
}
