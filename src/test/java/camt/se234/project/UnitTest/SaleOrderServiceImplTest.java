package camt.se234.project.UnitTest;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import camt.se234.project.service.SaleOrderServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;

    @Before
    public void setup() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);
    }

    @Test
    public void testGetSaleOrders() {
        List<SaleTransaction> mockTransactions1 = new ArrayList<>();
        List<SaleTransaction> mockTransactions2 = new ArrayList<>();
        mockTransactions1.add(new SaleTransaction("t001", new SaleOrder("o001", mockTransactions1),new Product("p0001", "Garden", "VThe garden which you can grow everything on earth", "garden.jpg", 20000.0), 1));
        mockTransactions1.add(new SaleTransaction("t002", new SaleOrder("o001", mockTransactions1),new Product("p0004", "Papaya", "Use for papaya salad", "papaya.jpg", 12.0), 10));
        mockTransactions2.add(new SaleTransaction("t003", new SaleOrder("o002", mockTransactions2),new Product("p0002", "Banana", "A good fruit with very cheap price", "banana.jpg", 150.0), 2));
        mockTransactions2.add(new SaleTransaction("t004", new SaleOrder("o002", mockTransactions2),new Product("p0001", "Garden", "The garden which you can grow everything on earth", "garden.jpg", 20000.0), 3));

        List<SaleOrder> mockOrders = new ArrayList<>();
        mockOrders.add(new SaleOrder("o001", mockTransactions1));
        mockOrders.add(new SaleOrder("o002", mockTransactions2));
        when(orderDao.getOrders()).thenReturn(mockOrders);
        assertThat(saleOrderService.getSaleOrders(), hasItems(new SaleOrder("o001", mockTransactions1),new SaleOrder("o002", mockTransactions2)));
        assertThat(saleOrderService.getSaleOrders().size(),is(2));
    }

    @Test
    public void testGetAverageSaleOrderPrice() {
        List<SaleTransaction> mockTransactions1 = new ArrayList<>();
        List<SaleTransaction> mockTransactions2 = new ArrayList<>();
        mockTransactions1.add(new SaleTransaction("t001", new SaleOrder("o001", mockTransactions1),new Product("p0001", "Garden", "VThe garden which you can grow everything on earth", "garden.jpg", 20000.0), 1));
        mockTransactions1.add(new SaleTransaction("t002", new SaleOrder("o001", mockTransactions1),new Product("p0004", "Papaya", "Use for papaya salad", "papaya.jpg", 12.0), 10));
        mockTransactions2.add(new SaleTransaction("t003", new SaleOrder("o002", mockTransactions2),new Product("p0002", "Banana", "A good fruit with very cheap price", "banana.jpg", 150.0), 2));
        mockTransactions2.add(new SaleTransaction("t004", new SaleOrder("o002", mockTransactions2),new Product("p0001", "Garden", "The garden which you can grow everything on earth", "garden.jpg", 20000.0), 3));

        List<SaleOrder> mockOrders = new ArrayList<>();
        mockOrders.add(new SaleOrder("o001", mockTransactions1));
        mockOrders.add(new SaleOrder("o002", mockTransactions2));
        when(orderDao.getOrders()).thenReturn(mockOrders);
        assertThat(saleOrderService.getAverageSaleOrderPrice(), is(40210.0));
    }
}