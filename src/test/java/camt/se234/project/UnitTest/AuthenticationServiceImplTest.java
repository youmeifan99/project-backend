package camt.se234.project.UnitTest;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class AuthenticationServiceImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticationService;

    @Before
    public void setup() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }

    @Test
    public void testAuthenicateWithMock() {
        when(userDao.getUser("mya", "mya")).thenReturn(new User("mya", "mya", "student"));
        assertThat(authenticationService.authenticate("mya", "mya"), is(new User("mya", "mya", "student")));
        assertThat(authenticationService.authenticate("mya", "mya"), is(notNullValue()));

    }
}
